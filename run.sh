#source /opt/aarch64/hwhpc/bashrc_kp
source ../profile
#mpirun -np $1 -bind-to socket ./ExaMiniMD -il ../input/in.lj --comm-type MPI --kokkos-threads=12
export OMP_PROC_BIND=spread 
export OMP_PLACES=threads
echo $OMP_PROC_BIND
#mpirun  -H node01:32,node02:32 --allow-run-as-root -np $1 --report-bindings -bind-to core ./ExaMiniMD -il ../input/in.lj --comm-type MPI --kokkos-threads=1
#mpirun  --allow-run-as-root -np $1 --report-bindings -bind-to core ./ExaMiniMD -il ../input/in.lj --comm-type MPI --kokkos-threads=1
mpirun --allow-run-as-root -np $1 --bind-to $3 --map-by $3 --report-bindings --mca pml ucx --mca btl ^vader,tcp,openib,uct --mca coll ^hcoll -x UCX_RNDV_THRESH=4k -x UCX_TLS=shm  ./ExaMiniMD -il ./input/in_$2.lj --comm-type MPI --kokkos-threads=1
mkdir -p json/$3/$2
mv *.json json/$3/$2
