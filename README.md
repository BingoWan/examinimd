### ExaminiMD   
    ExaMiniMD是分子动力学模拟代码（如LAMPPS）的代理应用程序，与之前的 MD 代理应用程序（MiniMD、COMD）相比，其设计明显更加模块化，方便与硬件供应商协同开发，更好的促进分子动力学的研究和开发[1]，近年来，ExaMiniMD由于其易用性和良好的性能广泛运用于生物大分子结构和功能邻域中。目前，ExaMiniMD 实现了基本的 Lennard Jones 二体相互作用势以及 SNAP多体相互作用势[2]，这两种作用势也均能LAMPPS中使用。此外，ExaMiniMD 使用 Kokkos 编程模型来实现性能可移植性，并且能够在所有主要HPC平台（GPU、ManyCore、CPU）上运行。此代码来源于https://github.com/ECP-copa/ExaMiniMD，并将该代码移植到华为鲲鹏平台进行性能测试。
#### 编译运行  
- 1.	编译器及mpi
    鲲鹏：gcc的版本为9.2.0, mpi版本为4.0.2rc3
    X86:gcc版本5.3.0，mpi版本为4.0.2rc3
- 2.	编译Kokkos
    ExaMiniMD底层代码都是调用Kokkos框架里的库，所以要先安装Kokkos：  
    - 设置编译脚本cmake.sh⾥相关路径并将编译选项设置为-O3：  
    - 修改相关路径后，运⾏sh cmake.sh
- 3.	编译编译ExaMiniMD,进⼊src⽬录，修改Makefile中KOKKOS_PATH和编译选项，然后make
- 4.	ExaMiniMD的运行  
    在./src目录下执行：
    mpirun -np 2 -bind-to socket ./ExaMiniMD -il ../input/in.lj --comm-type MPI --kokkos-threads=12
- 5.	kokkos的profiling的⼯具使用
    - 进⼊kokkos-tools/profiling⽬录下，根据需求编译不同的profiling⼯具的动态库。
    - 设置环境变量KOKKOS_PROFILE_LIBRARY，例如： export KOKKOS_PROFILE_LIBRARY=/opt/aarch64/hwhpc/ExaMiniMD/kp/kokkostools/profiling/simple-kernel-timer-json/kp_kernel_timer.so
    - 程序运⾏完后在 src/json⽬录下有相应的性能⽇志⽂件。
