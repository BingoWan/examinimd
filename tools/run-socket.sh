source /opt/aarch64/hwhpc/bashrc_kp
source ../profile
#mpirun -np $1 -bind-to socket ./ExaMiniMD -il ../input/in.lj --comm-type MPI --kokkos-threads=12
export OMP_PROC_BIND=spread 
export OMP_PLACES=threads
echo $OMP_PROC_BIND
#mpirun -np $1 -bind-to socket ./ExaMiniMD -il ../input/in.lj --comm-type MPI 
#mpirun -np $1 --map-by numa --bind-to core --mca pml ucx --mca btl ^vader,tcp,openib,uct -x UCX_RNDV_THRESH=4k  ./ExaMiniMD -il ../input/in.lj --comm-type MPI 
#mpirun -np $1 -bind-to socket ./ExaMiniMD -il ../input/in.lj --comm-type MPI --kokkos-threads=12
#mpirun  --allow-run-as-root -np $1 --report-bindings -bind-to socket ./ExaMiniMD -il ../input/in.lj --comm-type MPI --kokkos-threads=1
mpirun  --allow-run-as-root -np $1 --report-bindings -bind-to socket ./ExaMiniMD -il ../input/in.lj --comm-type MPI --kokkos-threads=1
#mpirun  --allow-run-as-root -np $1 --report-bindings -bind-to core ./ExaMiniMD -il ../input/in.lj --comm-type MPI 
mkdir -p json/socket/$1
mv *.json json/socket/$1
