#!/bin/bash

if [[ $# -ne 1 ]]; then
	echo "$0 <timestamp>"
	exit 1
fi

mkdir -p exalog.$1
#mv *start* *stop* outfile.rank* exalog.$1
mv exalog.*.1.$1 exalog.*.2.$1 exalog.*.3.$1 exalog.*.4.$1 exalog.$1
tar -cf exalog.$1.tar exalog.$1
realpath exalog.$1.tar

