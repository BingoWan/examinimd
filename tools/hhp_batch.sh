#!/bin/bash

if uname -m | grep "x86" >/dev/null; then
    echo "You are using x86 machine. (Huang Haopeng)"
    exit 1
fi

MAX=4
#CORES=(96 92 88 84 80 76 72 68 64 60 56 52 48 44 40 36 32 28 24)
CORES=(96 88 80 72 64 56 48 40 32 24)
#CORES=(96)

WORK_ROOT="/opt/aarch64/hwhpc/ExaMiniMD/kp/ExaMiniMD/src"
EXE="$WORK_ROOT/ExaMiniMD"

SUFFIX=$(date +%y%m%d-%H%M%S)
OPT="--allow-run-as-root -bind-to core --report-bindings"
OPT="$OPT --mca pml ucx --mca btl ^vader,openib,uct --mca coll ^hcoll -x UCX_RNDV_THRESH=4k -x UCX_TLS=shm"


source /opt/aarch64/hwhpc/bashrc_kp
source ../profile
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
echo "Running $EXE"

doit() {
    if [[ $# -ne 2 ]]; then
        echo "Wrong argument number $#"
        return 1
    fi
    local CORE=$1
    local ID=$2

    echo "mpirun $OPT -np $CORE $EXE -il $WORK_ROOT/../input/in.lj --comm-type MPI --kokkos-threads=1"
    mpirun $OPT -np $CORE $EXE -il $WORK_ROOT/../input/in.lj --comm-type MPI --kokkos-threads=1 > stdout 2> stderr

    mkdir -p json/core/$CORE
    mv *.json json/core/$CORE

    local LOG_NAME="exalog.$CORE.$ID.$SUFFIX"
    local LOG="$WORK_ROOT/$LOG_NAME"
    mkdir -p $LOG
    mv stdout stderr *start* *stop* outfile.rank* $LOG
}

for i in $(seq 1 $MAX); do

    for CORE in ${CORES[@]}; do
        echo ">>> ($i/$MAX) Working on $CORE"
        doit $CORE $i
    done

done

